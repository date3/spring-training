package com.datev.springtraining.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketController {

    @Autowired
    TicketService ticketService;

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseTicketResponse> purchaseTicket(@RequestBody PurchaseRequest request) {
        Ticket ticket = ticketService.purchase(request);

        return ResponseEntity.ok(PurchaseTicketResponse.from(ticket));
    }

    @PostMapping("/return")
    public ResponseEntity<ReturnTicketResponse> returnTicket(@RequestBody ReturnTicketRequest request){
        Ticket ticket = ticketService.returnTicket(request);

        return ResponseEntity.ok(ReturnTicketResponse.from(ticket));
    }

}