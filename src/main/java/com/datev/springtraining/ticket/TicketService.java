package com.datev.springtraining.ticket;

import com.datev.springtraining.seat.NumberInvalid;
import com.datev.springtraining.seat.Seat;
import com.datev.springtraining.seat.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    SeatRepository seatRepository;

    public Ticket purchase(PurchaseRequest purchase) {
        if (purchase.isRowInvalid() || purchase.isColumnInvalid()) {
            throw new NumberInvalid("The number of a row or a column is out of bounds!");
        }

        if (isSeatAvailable(purchase)) {
            markSeatAsOccupied(purchase);
        } else {
            throw new AlreadyBooked("The ticket has been already purchased!");
        }

        return ticketRepository.save(Ticket.forSeat(getSeat(purchase)));
    }

    private void markSeatAsOccupied(PurchaseRequest purchase) {
        Seat seat = getSeat(purchase);
        seat.markAsOccupied();

        seatRepository.save(seat);
    }

    public Ticket returnTicket(ReturnTicketRequest request) {
        Ticket ticket = markTicketAsReturned(request);

        Seat seat = getSeatForTicket(ticket);
        seat.markAsAvailable();

        return ticket;
    }

    private Ticket markTicketAsReturned(ReturnTicketRequest request) {
        Ticket ticket = findTicketForRequest(request);
        ticket.markAsReturned();
        ticketRepository.save(ticket);
        return ticket;
    }

    private Ticket findTicketForRequest(ReturnTicketRequest request) {
        Optional<Ticket> ticket = ticketRepository.findByToken(request.getToken());
        if (ticket.isEmpty() || ticket.get().isExpired()) {
            throw new WrongToken();
        }
        return ticket.get();
    }

    public boolean isSeatAvailable(PurchaseRequest request) {
        return seatRepository.findAll().stream().filter(s -> s.matchesPurchase(request)).findFirst().orElseThrow().isAvailable();
    }

    public Seat getSeat(PurchaseRequest request) {
        return seatRepository.findAll().stream().filter(s -> s.matchesPurchase(request)).findFirst().orElseThrow();
    }

    private Seat getSeatForTicket(Ticket ticket) {
        return seatRepository.findAll().stream().filter(s -> s.equals(ticket.getBookedSeat())).findFirst().orElseThrow();
    }

    public void setSeatRepository(SeatRepository seatRepository) {
        this.seatRepository = seatRepository;
    }
}
