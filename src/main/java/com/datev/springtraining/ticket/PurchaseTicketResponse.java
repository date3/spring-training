package com.datev.springtraining.ticket;

public class PurchaseTicketResponse {

    private String token;

    private int row, column, price;

    public static PurchaseTicketResponse from(Ticket ticket) {
        PurchaseTicketResponse response = new PurchaseTicketResponse();
        response.row = ticket.getBookedSeat().getRow();
        response.column = ticket.getBookedSeat().getRow();
        response.price = ticket.getPrice();
        response.token = ticket.getToken();

        return response;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
