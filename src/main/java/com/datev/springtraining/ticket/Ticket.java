package com.datev.springtraining.ticket;

import com.datev.springtraining.seat.Seat;

import java.util.UUID;

public class Ticket {

    private String token;

    private Seat bookedSeat;

    int price;

    boolean expired;

    public static Ticket forSeat(Seat seat) {
        Ticket response = new Ticket();
        response.token = UUID.randomUUID().toString();
        response.bookedSeat = seat;
        response.expired = false;
        response.price = new DefaultPricing().calculatePriceFor(seat);
        return response;
    }

    public String getToken() {
        return token;
    }

    public Seat getBookedSeat() {
        return bookedSeat;
    }

    public void setBookedSeat(Seat bookedSeat) {
        this.bookedSeat = bookedSeat;
    }

    public boolean isExpired() {
        return this.expired;
    }

    public void markAsReturned() {
        this.expired = true;
    }

    public int getPrice() {
        return this.price;
    }

    public static class DefaultPricing {
        public static final int EXPENSIVE_SEAT_ROW = 4;
        public static final int EXPENSIVE_PRICE = 10;
        public static final int CHEAP_PRICE = 8;

        public int calculatePriceFor(Seat seat) {
            if (seat.getRow() <= EXPENSIVE_SEAT_ROW) {
                return EXPENSIVE_PRICE;
            }
            return CHEAP_PRICE;
        }
    }
}
