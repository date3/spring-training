package com.datev.springtraining.ticket;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AlreadyBooked extends RuntimeException {

    String error;

    public AlreadyBooked(String s) {
        error = s;
    }
}
