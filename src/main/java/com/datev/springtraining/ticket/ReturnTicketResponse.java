package com.datev.springtraining.ticket;

import com.datev.springtraining.seat.Seat;

public class ReturnTicketResponse {

    ReturnedTicketResponse returned_ticket;

    public static ReturnTicketResponse from(Ticket ticket) {
        ReturnTicketResponse response = new ReturnTicketResponse();
        Seat bookedSeat = ticket.getBookedSeat();

        response.returned_ticket = new ReturnedTicketResponse(bookedSeat.getRow(), bookedSeat.getColumn(), ticket.getPrice());

        return response;
    }

    static class ReturnedTicketResponse {
        int row, column, price;

        public ReturnedTicketResponse(int row, int column, int price) {
            this.row = row;
            this.column = column;
            this.price = price;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }
    }

    public ReturnedTicketResponse getReturned_ticket() {
        return returned_ticket;
    }

    public void setReturned_ticket(ReturnedTicketResponse returned_ticket) {
        this.returned_ticket = returned_ticket;
    }
}
