package com.datev.springtraining.ticket;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TicketRepository {

    // TODO: just for training purposes. Don´t do that in production, needs to be refactored during training session.
    private List<Ticket> tickets = new ArrayList<>();

    Optional<Ticket> findByToken(String token) {
        return this.tickets.stream().filter(t -> t.getToken().equals(token)).findFirst();
    }

    public List<Ticket> findAll() {
        return tickets;
    }

    public Ticket save(Ticket ticket) {
        tickets.add(ticket);
        return ticket;
    }

    @PostConstruct
    public void resetTickets(){
        tickets = new ArrayList<>();
    }
}
