package com.datev.springtraining.ticket;

public class PurchaseRequest {

    int row, column;

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public boolean isColumnInvalid() {
        return (column < 1 || column > 9);
    }

    public boolean isRowInvalid() {
        return ((row < 1 || row > 9));
    }
}
