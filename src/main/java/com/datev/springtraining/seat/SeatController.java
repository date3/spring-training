package com.datev.springtraining.seat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeatController {

    @Autowired
    private SeatService seatService;

    @GetMapping("/seats")
    public SeatsResponse getSeats() {
        var response = new SeatsResponse();
        response.available_seats = seatService.getAvailableSeats();
        response.total_columns = 9;
        response.total_rows = 9;

        return response;
    }

}
