package com.datev.springtraining.seat;

import com.datev.springtraining.ticket.PurchaseRequest;

import javax.persistence.*;

@Entity
public class Seat {

    @Id
    @GeneratedValue
    int id;

    @Column(name = "seat_row")
    int row;

    @Column(name = "seat_column")
    int column;

    @Column
    boolean occupied;

    @SuppressWarnings("unused")
    private Seat(){}

    public Seat(int row, int column) {
        this.row = row;
        this.column = column;
        this.occupied = false;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean matchesPurchase(PurchaseRequest request) {
        return getRow() == request.getRow() && getColumn() == request.getColumn();
    }

    public void markAsOccupied() {
        this.occupied = true;
    }

    public boolean isAvailable() {
        return !this.occupied;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat seat = (Seat) o;

        if (row != seat.row) return false;
        if (column != seat.column) return false;
        return occupied == seat.occupied;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        result = 31 * result + (occupied ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "id=" + id +
                ", row=" + row +
                ", column=" + column +
                ", occupied=" + occupied +
                '}';
    }

    public void markAsAvailable() {
        this.occupied = false;
    }
}
