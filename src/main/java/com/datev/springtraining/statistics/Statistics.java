package com.datev.springtraining.statistics;

import com.datev.springtraining.seat.Seat;
import com.datev.springtraining.ticket.Ticket;

import java.util.List;

public class Statistics {

    private long currentIncome, availableSeats, numberOfPurchasedTickets;

    public static Statistics from(List<Ticket> tickets, List<Seat> seats) {
        var stats = new Statistics();
        stats.currentIncome = tickets.stream().mapToInt(Ticket::getPrice).sum();
        stats.availableSeats = seats.stream().filter(Seat::isAvailable).count();
        stats.numberOfPurchasedTickets = tickets.stream().filter(t -> !t.isExpired()).count();

        return stats;
    }

    public long getCurrentIncome() {
        return currentIncome;
    }

    public long getAvailableSeats() {
        return availableSeats;
    }

    public long getNumberOfPurchasedTickets() {
        return numberOfPurchasedTickets;
    }
}
