package com.datev.springtraining.statistics;

public class StatisticsResponse {

    private long current_income, number_of_available_seats, number_of_purchased_tickets;

    public static StatisticsResponse from(Statistics statistics) {
        var response = new StatisticsResponse();
        response.setNumber_of_available_seats(statistics.getAvailableSeats());
        response.setCurrent_income(statistics.getCurrentIncome());
        response.setNumber_of_purchased_tickets(statistics.getNumberOfPurchasedTickets());

        return response;
    }

    public long getCurrent_income() {
        return current_income;
    }

    public void setCurrent_income(long current_income) {
        this.current_income = current_income;
    }

    public long getNumber_of_available_seats() {
        return number_of_available_seats;
    }

    public void setNumber_of_available_seats(long number_of_available_seats) {
        this.number_of_available_seats = number_of_available_seats;
    }

    public long getNumber_of_purchased_tickets() {
        return number_of_purchased_tickets;
    }

    public void setNumber_of_purchased_tickets(long number_of_purchased_tickets) {
        this.number_of_purchased_tickets = number_of_purchased_tickets;
    }
}
