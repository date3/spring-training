package com.datev.springtraining.statistics;

import com.datev.springtraining.seat.SeatRepository;
import com.datev.springtraining.ticket.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Value("${com.datev.springtraining.stats.password}")
    private String superSecretPassword;

    @GetMapping("/stats")
    public ResponseEntity<StatisticsResponse> getStatistics(@RequestParam(value = "password", defaultValue = "") String password) {
        if(!password.equals(superSecretPassword)){
            throw new PasswordWrong();
        }

        var statistics = Statistics.from(ticketRepository.findAll(), seatRepository.findByOccupiedFalse());
        return ResponseEntity.ok(StatisticsResponse.from(statistics));
    }

    @PostMapping("/reset")
    public void resetTickets(){
        ticketRepository.resetTickets();
    }
}
