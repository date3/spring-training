# Aufgabe
Die Tickets werden leider noch nicht in der Datenbank gespeichert. Deine Aufgabe ist es, das `TicketRepository`  und `Ticket` so umzubauen, das die Tickets
in der Tabelle `ticket` gespeichert werden. Die notwendigen DB Tabellen sind bereits vorhanden. Überprüfe danach auch, ob du danach überflüssigen Code entfernen
kannst.