package week1;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Sie bilden ausschließlich natürliche Zahlen ab, allerdings ohne 0 (Null).
 * Die Grundzahlen (I, X, C, M) dürfen nicht mehr als dreimal nebeneinander geschrieben werden.
 * Die Zwischenzahlen (V, L, D) dürfen nur einmal nebeneinander stehen.
 * Die Zahlen I, X und C dürfen lediglich von einem der der beiden nächst höheren Zahlen abgezogen werden.
 * Das heißt: XL für 40 ist erlaubt, aber IL für vermeintlich 39 nicht. Die 39 schreibt sich korrekt XXXIX.
 * <p>
 * I 	1
 * IV   4
 * V 	5
 * IX   9
 * X 	10
 * L 	50
 * XC 	90
 * C 	100
 * D 	500
 * CM 	900
 * M 	1000
 */
public class RomanNumeralsTest {


}

