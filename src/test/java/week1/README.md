# String Calculator Kata

Das Ziel dieser Kata ist es, den Ablauf von TDD zu üben. Das Original stammt von Roy Overshore. Die Übung zeit außerdem, wie wichtig kleine Schritte in der Entwicklung sind. 

[String Calculator Kata](https://osherove.com/tdd-kata-1)

Für die Ausführung der Kata gibt es ein paar Regeln:

* Bearbeite die Kata Schritt für Schritt - beginnend bei Aufgabe 1. 
* Lese immer nur bis zur nächsten Aufgabe.
* Du brauchtst nur gültige Eingaben zu testen.
* Schreibe immer(!) zuerst einen Test!

## Aufgabe

Entwickle mit TDD eine Methode, die verschiedene Zahlen als `String` bekommt und eine Summe als `Integer` zurückgibt. 

## Ablauf

1. Schreibe eine Test für die Methode `StringCalculator.add("")`. Die Methode hat einen Parameter mit dem Typ String. Für einen leeren String gibt die Methode `0` zurück. Schreibe zuerst den  Test! Schreibe den denkbar einfachsten Test!

2. Die Methode soll bei der Eingabe einer Zahl z.B. `"5"` die Summe `5` zurückgeben.

3. Die Methode soll bei der Eingabe von zwei Zahlen die die Summe zurückgeben. Beispiel: `"7,3"` -> `10`

4. Die Methode unterstützt eine variable Anzahl von Zahlen. Beispiel: `"6,4"`, `"10,6,4"`

5. Die Methode unterstützt Zeilenumbrüche und Komata. Gehe davon aus, dass die Eingabe immer gültig ist, du brauchst nicht auf ungültige Eingaben zu Testen. Beispiel: `"6\n4,5"` -> `15`

6. Die Methode unterstützt verschiedene Trenner. Der gewählte Trenner wird in einer separaten Zeile am Anfang in der Form `//[delimeter]\n[numbers]` angegeben. Die erste Zeile ist optional, alle bisherigen Szenarien müssen weiterhin unterstützt werden. Beispiel: `"//;\n5;8;7"` -> `20`

7. Bei einer negativen Zahlen erzeugt die Methode eine Exception mit der Nachricht `Negative numbers not allowed: [negative number]` Die Nachricht enthält die gefundene negative Zahl. Beispiel: `"//;\n6;7;-9"` -> `Negative numbers not allowed: -9`

8. Bei mehreren negativen Zahlen enthält die Exception alle negativen Zahlen. Beispiel:  `"//;\n6;7;-9;-10"` -> `Negative numbers not allowed: -9, -10`

9. Zahlen, die größer als 1000 sind, werden von der Methode ignoriert. Beispiel: `"5,6,1000"` -> `11`

10. Der Trenner kann eine beliebige Länge haben. Beispiel: `"//[***]\n6***8***1"` -> `15`

11. Es können mehrere Trenner angegeben werden. Beispiel: `"//[*][$]\n5*5$4"` -> `14`

12. Verschiedene Trenner können unterschiedlich lang sein. Beispiel: `"//[***][$]\n5***5$4"` -> `14`