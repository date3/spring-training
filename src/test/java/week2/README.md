# Expense Report Legacy Kata
Die Expense Report Kata ist eine Legacy Code Kata. Ziel der Aufgabe ist es, ungetestenen Code zu testen und danach durch verschiedene Refactorings zu verbessern.

Das Original findet sich hier: [Expense Report](https://github.com/christianhujer/expensereport)

## Aufgabe
Schreibe einen ersten Test um das aktuellen Verhalten aufzuzeichnen. Leider hat der ursprünglich Autor nicht auf die Testbarkeit des Systems geachtet. Um das aktuelle Verhalten zu testen musst die die Ausgaben von `System.out` abfangen.

Glücklicherweise hast du ein nützliches Werkzeug in Spring Boot gefunden was Dir dabei hilft und Du nun den ersten "Golden Master" Test schreiben kannst.

## Ablauf

1. Schreibe den ersten "Golden Master Test" ohne die Implementierung zu verändern! Führe den Test aus und teste verschiedene Eingaben.

2. Führe ein erstes Refactoring durch, mit dem Ziel, den Implementierung ohne Ausgabeumleitung testen zu können.

3. Teste ausführlich die neu erzeugte Schnittstelle.

4. Führe weitere Refactorings durch.

5. Füge ein lang ersehntes Feature hinzu: das Programm soll eine neue Ausgabe mit dem Typ `Lunch` und einem Limit von `2000` unterstützen.