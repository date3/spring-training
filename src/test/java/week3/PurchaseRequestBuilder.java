package week3;

import com.datev.springtraining.ticket.PurchaseRequest;

public class PurchaseRequestBuilder {

    int column, row;

    public PurchaseRequestBuilder(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static PurchaseRequestBuilder aPurchaseRequest() {
        return new PurchaseRequestBuilder(5, 5);
    }

    public PurchaseRequestBuilder withRow(int row){
        this.row = row;
        return this;
    }

    public PurchaseRequestBuilder withColumn(int column){
        this.column = column;
        return this;
    }

    public PurchaseRequest build(){
        PurchaseRequest purchaseRequest = new PurchaseRequest();
        purchaseRequest.setRow(row);
        purchaseRequest.setColumn(column);
        return purchaseRequest;
    }

}
