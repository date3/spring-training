package week3.unit;

import com.datev.springtraining.seat.Seat;
import com.datev.springtraining.statistics.Statistics;
import com.datev.springtraining.ticket.Ticket;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static week3.SeatBuilder.aSeat;
import static week3.TicketBuilder.aTicket;

public class StatisticsTest {

    @Test
    public void newStatistics_noTickets_currentIncomeIs0() {
        var statistics = newStatistics(of(), of());

        assertThat(statistics.getCurrentIncome()).isEqualTo(0);
    }

    private Statistics newStatistics(List<Ticket> tickets, List<Seat> seats) {
        return Statistics.from(tickets, seats);
    }

    @Test
    public void newStatistics_ticketFor10_currentIncomeIs10() {
        var statistics = newStatistics(of(aTicket().withSeat(aSeat().withRow(2)).build()), of());

        assertThat(statistics.getCurrentIncome()).isEqualTo(10);
    }

    @Test
    public void newStatistics_ticketFor10andTicketFor8_currentIncomeIs18() {
        var statistics = newStatistics(asList(
                        aTicket().withSeat(aSeat().withRow(2)).build(),
                        aTicket().withSeat(aSeat().withRow(5)).build()),
                of()
        );

        assertThat(statistics.getCurrentIncome()).isEqualTo(18);
    }

    @Test
    public void newStatistics_1availableSeat_return1AvailableSeat() {
        var statistics = newStatistics(of(), of(aSeat().asAvailable().build()));

        assertThat(statistics.getAvailableSeats()).isEqualTo(1);
    }

    @Test
    public void newStatistics_2Seats1Occupied_return1AvailableSeat() {
        var statistics = newStatistics(of(), of(aSeat().asAvailable().build(), aSeat().asOccupied().build()));

        assertThat(statistics.getAvailableSeats()).isEqualTo(1);
    }

    @Test
    public void newStatistics_noTicketsPurchased_numberOfPurchasedTicketsIs0() {
        var statistics = newStatistics(of(), of());

        assertThat(statistics.getNumberOfPurchasedTickets()).isEqualTo(0);
    }

    @Test
    public void numberOfPurchasedTickets_1purchasedTicket_numberOfPurchasedTicketsIs1() {
        var statistics = newStatistics(of(aTicket().build()), of());

        assertThat(statistics.getNumberOfPurchasedTickets()).isEqualTo(1);
    }

    @Test
    public void numberOfPurchasedTickets_1purchasedTicket1IsReturned_numberOfPurchasedTicketsIs1() {
        var statistics = newStatistics(of(aTicket().build(), aTicket().asReturned().build()), of());

        assertThat(statistics.getNumberOfPurchasedTickets()).isEqualTo(1);
    }
}
