package week3.unit;

import com.datev.springtraining.seat.NumberInvalid;
import com.datev.springtraining.seat.SeatRepository;
import com.datev.springtraining.ticket.AlreadyBooked;
import com.datev.springtraining.ticket.TicketService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static week3.PurchaseRequestBuilder.aPurchaseRequest;
import static week3.SeatBuilder.aSeat;

public class TicketServiceTest {

    private TicketService ticketService;

    private SeatRepository seatRepository;

    @BeforeEach
    public void beforeEach(){
        seatRepository = mock(SeatRepository.class);

        ticketService = new TicketService();
        ticketService.setSeatRepository(seatRepository);
    }

    @Test
    public void markSeatAsOccupied_rowGt9_numberInvalidIsThrown() {
        Throwable exception = catchThrowable(() -> purchaseTicketFor(10, 5));

        assertThat(exception).isInstanceOf(NumberInvalid.class);
    }

    @Test
    public void markSeatAsOccupied_columnGt9_numberInvalidIsThrown() {
        Throwable exception = catchThrowable(() -> purchaseTicketFor(9, 10));

        assertThat(exception).isInstanceOf(NumberInvalid.class);
    }

    @Test
    public void markSeatAsOccupied_rowLt0_numberInvalidIsThrown() {
        Throwable exception = catchThrowable(() -> purchaseTicketFor(-1, 5));

        assertThat(exception).isInstanceOf(NumberInvalid.class);
    }

    @Test
    public void markSeatAsOccupied_columnLt0_numberInvalidIsThrown() {
        Throwable exception = catchThrowable(() -> purchaseTicketFor(9, -1));

        assertThat(exception).isInstanceOf(NumberInvalid.class);
    }

    @Test
    public void markSeatAsOccupied_alreadyBooked_exceptionIsThrown() {
        when(seatRepository.findAll()).thenReturn(of(aSeat().withRow(5).withColumn(5).asOccupied().build()));
        Throwable exception = catchThrowable(() -> purchaseTicketFor(5, 5));

        assertThat(exception).isInstanceOf(AlreadyBooked.class);
    }

    private void purchaseTicketFor(int row, int column) {
        ticketService.purchase(aPurchaseRequest().withRow(row).withColumn(column).build());
    }
}
