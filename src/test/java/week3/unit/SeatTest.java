package week3.unit;

import com.datev.springtraining.ticket.PurchaseRequest;
import com.datev.springtraining.seat.Seat;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static week3.PurchaseRequestBuilder.aPurchaseRequest;
import static week3.SeatBuilder.aSeat;

public class SeatTest {

    @Test
    void matchesPurchase_doesMatch_true(){
        Seat seat = aSeat().withColumn(5).withRow(5).build();
        PurchaseRequest purchase = aPurchaseRequest().withRow(5).withColumn(5).build();

        assertThat(seat.matchesPurchase(purchase)).isTrue();
    }

    @Test
    void matchesPurchase_doesNotMatch_true(){
        Seat seat = aSeat().withColumn(6).withRow(5).build();
        PurchaseRequest purchase = aPurchaseRequest().withRow(5).withColumn(5).build();

        assertThat(seat.matchesPurchase(purchase)).isFalse();
    }
}
