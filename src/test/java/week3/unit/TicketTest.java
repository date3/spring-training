package week3.unit;

import com.datev.springtraining.ticket.Ticket;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static week3.SeatBuilder.aSeat;

public class TicketTest {

    @Test
    public void newTicketForSeat_row4_priceIs10(){
        var ticket = Ticket.forSeat(aSeat().withRow(4).build());
        assertThat(ticket.getPrice()).isEqualTo(10);
    }

    @Test
    public void newTicketForSeat_rowLt4_priceIs10(){
        var ticket = Ticket.forSeat(aSeat().withRow(3).build());
        assertThat(ticket.getPrice()).isEqualTo(10);
    }

    @Test
    public void newTicketForSeat_rowGt4_priceIs8(){
        var ticket = Ticket.forSeat(aSeat().withRow(5).build());
        assertThat(ticket.getPrice()).isEqualTo(8);
    }
}
