package week3.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.datev.springtraining.App;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = {App.class})
@AutoConfigureMockMvc
@Sql({"/schema.sql", "/data.sql"})
public class TicketControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    public void beforeEach() {
    }

    @Test
    public void purchase_purchaseIsValid_seatIsNotAvailable() {}

    @Test
    public void purchase_purchaseHasRowGt10_throwsHttp400() {}

    @Test
    public void returnTicket_validReturnOfAPurchasedTicket_valid() {}

    @Test
    public void returnTicket_alreadyReturnedTicket_returnsHttp400() {}

    @Test
    public void returnTicket_invalidToken_returnsHttp400() {}

}
