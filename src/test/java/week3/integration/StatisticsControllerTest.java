package week3.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.datev.springtraining.App;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = {App.class})
@AutoConfigureMockMvc
@Sql({"/schema.sql", "/data.sql"})
public class StatisticsControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getStatistics_noTicketSold_allSeatsAvailable() {}

    @Test
    public void getStatistics_wrongPasswordProvided_returnsHttp401() {}

    @Test
    public void getStatistics_noPasswordProvided_returnsHttp401() {}
}
