package week3.integration;

import com.datev.springtraining.App;
import com.datev.springtraining.seat.Seat;
import com.datev.springtraining.seat.SeatRepository;
import com.datev.springtraining.seat.SeatService;
import com.datev.springtraining.ticket.ReturnTicketRequest;
import com.datev.springtraining.ticket.Ticket;
import com.datev.springtraining.ticket.TicketRepository;
import com.datev.springtraining.ticket.TicketService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static week3.PurchaseRequestBuilder.aPurchaseRequest;
import static week3.SeatBuilder.aSeat;


@Transactional
@SpringBootTest(classes = {App.class})
@Sql(value = {"/schema.sql", "/data.sql"})
public class TicketServiceTest {

    @Autowired
    TicketService ticketService;

    @Autowired
    SeatService seatService;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    SeatRepository seatRepository;

    @Test
    public void purchase_purchaseAVacantSeat_returns80Seats() {
        purchaseTicketFor(1, 1);
        assertThat(seatService.getAvailableSeats()).hasSize(80);
    }

    @Test
    public void purchase_purchasedAVacantSeat_ticketIsSaved() {
        purchaseTicketFor(1, 1);
        assertThat(ticketRepository.findAll()).hasSize(1);
    }

    @Test
    public void returnTicket_validReturn_occupiedSeatIsVacantAgain() {
        Ticket ticket = purchaseTicketFor(1, 1);

        ticketService.returnTicket(new ReturnTicketRequest(ticket.getToken()));

        Seat seat = aSeat().withRow(1).withColumn(1).build();
        assertThat(seatRepository.findByOccupiedFalse()).contains(seat);
    }

    @Test
    public void getAvailableSeats_occupiedSeatInRow5Column5_seatIsNotReturned() {
        purchaseTicketFor(5, 5);
        List<Seat> availableSeats = seatService.getAvailableSeats();

        assertThat(availableSeats).doesNotContain(new Seat(5, 5));
    }

    private Ticket purchaseTicketFor(int row, int column) {
        return ticketService.purchase(aPurchaseRequest().withRow(row).withColumn(column).build());
    }
}
