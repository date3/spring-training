package week3;

import com.datev.springtraining.ticket.Ticket;

import java.util.UUID;

public class TicketBuilder {

    private UUID id;

    private String token;

    private SeatBuilder seat;

    boolean returned;

    private TicketBuilder(UUID id, String token, SeatBuilder seat, boolean returned) {
        this.id = id;
        this.token = token;
        this.seat = seat;
        this.returned = returned;
    }

    public static TicketBuilder aTicket() {
        return new TicketBuilder(UUID.randomUUID(), UUID.randomUUID().toString(), SeatBuilder.aSeat(), false);
    }

    public TicketBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public TicketBuilder withSeat(SeatBuilder seat) {
        this.seat = seat;
        return this;
    }

    public Ticket build() {
        Ticket ticket = Ticket.forSeat(seat.build());
        if (this.returned) {
            ticket.markAsReturned();
        }
        return ticket;
    }

    public TicketBuilder asReturned() {
        this.returned = true;
        return this;
    }
}
